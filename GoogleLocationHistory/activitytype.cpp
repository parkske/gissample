/*!
 * Copyright Kevin Parks 2017
 * \file activitytype.cpp
 */

#include "activitytype.h"

ActivityType::ActivityType(QString type, quint32 confidence)
{
    mType = type;
    mConfidence = confidence;
}

QString ActivityType::type() const
{
    return mType;
}

void ActivityType::setType(const QString &type)
{
    mType = type;
}

quint32 ActivityType::confidence() const
{
    return mConfidence;
}

void ActivityType::setConfidence(const quint32 &confidence)
{
    mConfidence = confidence;
}

void ActivityType::read(const QJsonObject &json)
{
    mType = json["type"].toString();
    mConfidence = json["confidence"].toInt();
}
