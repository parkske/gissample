/*!
 * Copyright Kevin Parks 2017
 * \file glocation.cpp
 */

#include "glocation.h"
#include "activity.h"
#include <QDebug>
#include <QJsonArray>
#include <QtMath>

#define POWER_DIVISOR (qPow(10.0, 7.0))

GLocation::GLocation(quint64 timeStampMs, qreal latitude, qreal longitude, qint32 accuracy)
{
    mTimeStampMs = timeStampMs;
    mLatitudeE7 = latitude;
    mLongitudeE7 = longitude;
    mAccuracy = accuracy;
}

GLocation::GLocation(quint64 timeStampMs, qreal latitude, qreal longitude, qint32 accuracy, QList<Activity> activitys)
{
    mTimeStampMs = timeStampMs;
    mLatitudeE7 = latitude;
    mLongitudeE7 = longitude;
    mAccuracy = accuracy;
    mActivitys = activitys;
}

quint64 GLocation::timeStampMs() const
{
    return mTimeStampMs;
}

void GLocation::setTimeStampMs(const quint64 &timeStampMs)
{
    mTimeStampMs = timeStampMs;
}

qreal GLocation::latitudeE7() const
{
    return mLatitudeE7;
}

void GLocation::setLatitudeE7(const qreal &latitudeE7)
{
    mLatitudeE7 = latitudeE7;
}

qreal GLocation::longitudeE7() const
{
    return mLongitudeE7;
}

void GLocation::setLongitudeE7(const qreal &longitudeE7)
{
    mLongitudeE7 = longitudeE7;
}

qint32 GLocation::accuracy() const
{
    return mAccuracy;
}

void GLocation::setAccuracy(const qint32 &accuracy)
{
    mAccuracy = accuracy;
}

QList<Activity> GLocation::activitys() const
{
    return mActivitys;
}

void GLocation::setActivitys(const QList<Activity> &activitys)
{
    mActivitys = activitys;
}


/*!
 * \brief GLocation::read
 * \param json The JSONObject to parse
 */
void GLocation::read(const QJsonObject &json)
{
    mTimeStampMs = json["timestampMs"].toString().toULongLong();
    // Google provides these at int = decimal x 10^7
    mLatitudeE7 = (qreal) ((json["latitudeE7"].toInt()) / POWER_DIVISOR);
    mLongitudeE7 = (qreal) ((json["longitudeE7"].toInt()) / POWER_DIVISOR);
    mAccuracy = json["accuracy"].toInt();
    // Determine if there are any activtys
    QJsonArray activitysArray = json["activitys"].toArray();
    for(int i = 0; i < activitysArray.size(); ++i)
    {
        QJsonObject object = activitysArray.at(i).toObject();
        Activity activity;
        activity.read(object);
        mActivitys.append(activity);
    }
}
