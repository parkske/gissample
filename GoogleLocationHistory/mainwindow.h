/*!
 * Copyright Kevin Parks 2017
 * \file mainwindow.h
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

namespace Esri
{
namespace ArcGISRuntime
{
class Map;
class MapGraphicsView;
class GraphicsOverlay;
}
}

#include <QMainWindow>
#include "glocation.h"
#include "MapTypes.h"
#include "Viewpoint.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    bool readFromJSON(const QString &filePath);

private slots:
    void on_pushButton_open_clicked();
    void on_pushButton_reset_clicked();
    void onViewpointChanged();

private:
    bool plotPoints();
    void reposition();
    void reset(bool resetView = false);
    Ui::MainWindow*                             ui;
    QList<GLocation>                            m_locations;
    Esri::ArcGISRuntime::Map*                   m_map;
    Esri::ArcGISRuntime::MapGraphicsView*       m_mapView;
    Esri::ArcGISRuntime::GraphicsOverlay*       m_overlay;
    Esri::ArcGISRuntime::Viewpoint              m_initialViewpoint;
    bool                                        m_initialViewpointSet;
};

#endif // MAINWINDOW_H
