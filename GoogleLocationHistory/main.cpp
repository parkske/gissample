/*!
 * Copyright Kevin Parks 2017
 * \file main.cpp
 */

#include "mainwindow.h"
#include <QApplication>

#include "ArcGISRuntimeEnvironment.h"

using namespace Esri::ArcGISRuntime;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
#ifdef Q_OS_WIN
    // Force usage of OpenGL ES through ANGLE on Windows
    QCoreApplication::setAttribute(Qt::AA_UseOpenGLES);
#endif
    MainWindow w;
    w.showMaximized();
    return a.exec();
}
