/*!
 * Copyright Kevin Parks 2017
 * \file activity.h
 */

#ifndef ACTIVITY_H
#define ACTIVITY_H

#include <QtGlobal>
#include <QList>

#include "activitytype.h"

class Activity
{
public:
    Activity(quint64 timeStampMs = 0);
    Activity(quint64 timeStampMs, QList<ActivityType> activityTypes);

    quint64 timeStampMs() const;
    void setTimeStampMs(const quint64 &timeStampMs);

    QList<ActivityType> activityTypes() const;
    void setActivityTypes(const QList<ActivityType> &activityTypes);

    void read(const QJsonObject &json);
private:
    quint64                 mTimeStampMs;
    QList<ActivityType>     mActivityTypes;
};

#endif // ACTIVITY_H
