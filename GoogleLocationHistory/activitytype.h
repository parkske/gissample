/*!
 * Copyright Kevin Parks 2017
 * \file activitytype.h
 */

#ifndef ACTIVITYTYPE_H
#define ACTIVITYTYPE_H

#include <QtGlobal>
#include <QString>
#include <QJsonObject>

class ActivityType
{
public:
    ActivityType(QString type = "", quint32 confidence = 0);

    QString type() const;
    void setType(const QString &type);

    quint32 confidence() const;
    void setConfidence(const quint32 &confidence);

    void read(const QJsonObject &json);
private:
    QString mType;
    quint32 mConfidence;
};

#endif // ACTIVITYTYPE_H
