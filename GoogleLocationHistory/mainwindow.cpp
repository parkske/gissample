/*!
 * Copyright Kevin Parks 2017
 * \file mainwindow.cpp
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QJsonDocument>
#include <QJsonArray>
#include <QFileDialog>
#include <QMessageBox>
#include <QProgressDialog>

#include "Basemap.h"
#include "Map.h"
#include "MapGraphicsView.h"
#include "Layer.h"
#include "ArcGISTiledLayer.h"
#include "SimpleMarkerSymbol.h"
#include "SimpleRenderer.h"
#include "Viewpoint.h"

#include "glocation.h"

using namespace Esri::ArcGISRuntime;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_initialViewpointSet(false)
{
    ui->setupUi(this);
    ui->label->setEnabled(false);

    // Create the Widget view
    m_mapView = new MapGraphicsView(this);
    m_mapView->setWrapAroundMode(WrapAroundMode::EnabledWhenSupported);

    // Create a map using the topographic BaseMap
    m_map = new Map(BasemapType::Topographic, 0.0, 0.0, 0, this);

    // Set map to map view
    m_mapView->setMap(m_map);
    connect(m_mapView, SIGNAL(viewpointChanged()), this, SLOT(onViewpointChanged()));

    // Setup the Graphics Overlay
    m_overlay = new GraphicsOverlay(this);
    SimpleMarkerSymbol* symbol = new SimpleMarkerSymbol(SimpleMarkerSymbolStyle::Circle, QColor(Qt::red), 5, this);
    SimpleRenderer *renderer = new SimpleRenderer(symbol, this);
    m_overlay->setRenderer(renderer);
    m_overlay->setRenderingMode(GraphicsRenderingMode::Static);

    // Position view in layout
    QVBoxLayout *layout = dynamic_cast<QVBoxLayout*>(this->ui->centralWidget->layout());
    if(layout)
    {
        layout->insertWidget(0, m_mapView, 1);
    }
    else
    {
        qFatal("Failed to add map to layout");
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

/*!
 * \brief MainWindow::readFromJSON
 * \param filePath The path to a Google JSON location history
 * \return true if file is parsed successfully & not cancelled, false otherwise
 */
bool MainWindow::readFromJSON(const QString &filePath)
{
    bool retval = true;

    // Minimum setup for progress dialog
    QProgressDialog progressDialog(this);
    progressDialog.setLabelText(tr("Parsing locations..."));
    progressDialog.setCancelButtonText(tr("Cancel"));
    progressDialog.setValue(0);
    progressDialog.setRange(0, 0);

    // File checking, opening
    if(!QFile::exists(filePath))
    {
        QMessageBox::critical(this, tr("Error"),
                              tr("Failed to find: %1").arg(filePath));
        return false;
    }
    QFile jsonFile(filePath);
    if(!jsonFile.open(QIODevice::ReadOnly))
    {
        QMessageBox::critical(this, tr("Error"),
                              tr("Failed to open: %1").arg(filePath));
        return false;
    }

    // Convert to JSON
    QByteArray jsonData = jsonFile.readAll();
    QJsonDocument jsonDocument(QJsonDocument::fromJson(jsonData));
    if(jsonDocument.isNull() || !jsonDocument.isObject())
    {
        QMessageBox::critical(this, tr("Error"),
                              tr("JSON data invalid"));
        return false;
    }

    // Begin parsing
    QJsonObject jsonObject = jsonDocument.object();
    QJsonArray jsonLocations = jsonObject["locations"].toArray();
    progressDialog.setMaximum(jsonLocations.count());
    for(int i = 0; i < jsonLocations.count(); ++i)
    {
        QApplication::processEvents();
        if(progressDialog.wasCanceled())
        {
            retval = false;
            break;
        }
        QJsonObject locationObject = jsonLocations.at(i).toObject();
        GLocation location;
        location.read(locationObject);
        m_locations.append(location);
        progressDialog.setValue(i);
    }
    progressDialog.setValue(progressDialog.maximum());

    return retval;
}

/*!
 * \brief MainWindow::plotPoints
 * \return true of plotting succeeds and wasn't cancelled, false otherwise
 */
bool MainWindow::plotPoints()
{
    bool retval = true;

    // Minimum setup for progress dialog
    QProgressDialog progressDialog(this);
    progressDialog.setLabelText(tr("Plotting locations..."));
    progressDialog.setCancelButtonText(tr("Cancel"));
    progressDialog.setValue(0);
    progressDialog.setRange(0, m_locations.count());

    // SpatialReference used
    SpatialReference sr(4326);

    // Plot each point
    foreach(GLocation location, m_locations)
    {
        QApplication::processEvents();
        if(progressDialog.wasCanceled())
        {
            retval = false;
            break;
        }
        Point point(location.longitudeE7(), location.latitudeE7(), sr);
        m_overlay->graphics()->append(new Graphic(point, this));
        progressDialog.setValue(progressDialog.value() + 1);
    }

    // Only if not cancelled and all went as expected should the layer be added
    if(retval)
    {
        // Add Graphics Layer
        m_mapView->graphicsOverlays()->append(m_overlay);
    }
    progressDialog.setValue(progressDialog.maximum());

    return retval;
}

/*!
 * \brief MainWindow::reposition
 */
void MainWindow::reposition()
{
    Viewpoint viewpoint(m_overlay->extent());
    m_mapView->setViewpointAnimated(viewpoint, 2, AnimationCurve::Linear);
}

/*!
 * \brief MainWindow::reset
 * \param resetView whether to reset the MapView
 */
void MainWindow::reset(bool resetView)
{
    ui->lineEdit->clear();
    m_mapView->graphicsOverlays()->clear();
    m_locations.clear();
    m_overlay->graphics()->clear();
    if(resetView)
        m_mapView->setViewpointAnimated(m_initialViewpoint, 2, AnimationCurve::Linear);
}
/*!
 * \brief MainWindow::on_pushButton_clicked
 */
void MainWindow::on_pushButton_open_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Select JSON Location File"),
                                                    QDir::homePath(),
                                                    tr("JSON (*.json)"));
    if(!fileName.isEmpty())
    {
        reset();
        ui->lineEdit->setText(fileName);
        if(readFromJSON(fileName))
        {
            // First plot, then reposition
            if(plotPoints())
                reposition();
        }
    }
}

/*!
 * \brief MainWindow::on_pushButton_reset_clicked
 */
void MainWindow::on_pushButton_reset_clicked()
{
    reset(true);
}

/*!
 * \brief MainWindow::onViewpointChanged
 */
void MainWindow::onViewpointChanged()
{
    if(!m_initialViewpointSet)
    {
        m_initialViewpoint = m_mapView->currentViewpoint(ViewpointType::BoundingGeometry);
        m_initialViewpointSet = true;
        disconnect(m_mapView, SIGNAL(viewpointChanged()), this, SLOT(onViewpointChanged()));
    }
}
