/*!
 * Copyright Kevin Parks 2017
 * \file activity.cpp
 */

#include "activity.h"
#include <QJsonArray>

Activity::Activity(quint64 timeStampMs)
{
    mTimeStampMs = timeStampMs;
}

Activity::Activity(quint64 timeStampMs, QList<ActivityType> activityTypes)
{
    mTimeStampMs = timeStampMs;
    mActivityTypes = activityTypes;
}

quint64 Activity::timeStampMs() const
{
    return mTimeStampMs;
}

void Activity::setTimeStampMs(const quint64 &timeStampMs)
{
    mTimeStampMs = timeStampMs;
}

QList<ActivityType> Activity::activityTypes() const
{
    return mActivityTypes;
}

void Activity::setActivityTypes(const QList<ActivityType> &activityTypes)
{
    mActivityTypes = activityTypes;
}

/*!
 * \brief Activity::read
 * \param json The JSONObject to parse
 */
void Activity::read(const QJsonObject &json)
{
    mTimeStampMs = json["timestampMs"].toString().toULongLong();
    // Determine if there are any ActivityTypes
    QJsonArray activitiesTypeArray = json["activities"].toArray();
    for(int i = 0; i < activitiesTypeArray.size(); ++i)
    {
        QJsonObject object = activitiesTypeArray.at(i).toObject();
        ActivityType activityType;
        activityType.read(object);
        mActivityTypes.append(activityType);
    }
}
