/*!
 * Copyright Kevin Parks 2017
 * \file glocation.h
 */

#ifndef LOCATION_H
#define LOCATION_H

#include <QtGlobal>
#include <QList>
#include <QJsonObject>

#include "activity.h"

class GLocation
{
public:
    GLocation(quint64 timeStampMs = 0, qreal latitude = 0,
             qreal longitude = 0, qint32 accuracy = 0);
    GLocation(quint64 timeStampMs, qreal latitude,
             qreal longitude, qint32 accuracy,
             QList<Activity> activitys);

    quint64 timeStampMs() const;
    void setTimeStampMs(const quint64 &timeStampMs);

    qreal latitudeE7() const;
    void setLatitudeE7(const qreal &latitudeE7);

    qreal longitudeE7() const;
    void setLongitudeE7(const qreal &longitudeE7);

    qint32 accuracy() const;
    void setAccuracy(const qint32 &accuracy);

    QList<Activity> activitys() const;
    void setActivitys(const QList<Activity> &activitys);

    void read(const QJsonObject &json);

private:
    quint64         mTimeStampMs;
    qreal           mLatitudeE7;
    qreal           mLongitudeE7;
    qint32          mAccuracy;
    QList<Activity> mActivitys;
};

#endif // LOCATION_H
