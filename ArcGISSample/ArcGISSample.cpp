// Copyright 2016 ESRI
//
// All rights reserved under the copyright laws of the United States
// and applicable international laws, treaties, and conventions.
//
// You may freely redistribute and use this sample code, with or
// without modification, provided you include the original copyright
// notice and use restrictions.
//
// See the Sample code usage restrictions document for further information.
//

#include "ArcGISSample.h"

#include "Basemap.h"
#include "Map.h"
#include "MapGraphicsView.h"
#include "Viewpoint.h"
#include <QGeoPositionInfoSource>
#include <QCompass>
#include <QDebug>
#include <QGraphicsSimpleTextItem>

using namespace Esri::ArcGISRuntime;

ArcGISSample::ArcGISSample(QWidget *parent /*=nullptr*/):
    QMainWindow(parent),
    m_map(nullptr),
    m_mapView(nullptr),
    m_centeredOnLocation(false),
    m_centralWidget(new QWidget(this)),
    m_layout(new QVBoxLayout(this)),
    m_long(new QLabel(this)),
    m_lat(new QLabel(this))
{

    // Create the Widget view
    m_mapView = new MapGraphicsView(this);
    m_mapView->setWrapAroundMode(WrapAroundMode::EnabledWhenSupported);

    // Create a map using the topographic BaseMap
    m_map = new Map(BasemapType::Topographic, 0.0, 0.0, 0);
    connect(m_map, SIGNAL(doneLoading(Esri::ArcGISRuntime::Error)), this, SLOT(startLocation()));

    // Set map to map view
    m_mapView->setMap(m_map);

    // Set up the Layout
    m_layout->addWidget(m_mapView);
    m_layout->addWidget(m_lat);
    m_layout->addWidget(m_long);
    m_centralWidget->setLayout(m_layout);

    // set the mapView as the central widget
    setCentralWidget(m_centralWidget);
}

// destructor
ArcGISSample::~ArcGISSample()
{

}

/*!
 * \brief ArcGISSample::startLocation
 */
void ArcGISSample::startLocation()
{
    m_mapView->locationDisplay()->setPositionSource(QGeoPositionInfoSource::createDefaultSource(this));
    QCompass* compass = new QCompass(this);
    m_mapView->locationDisplay()->setCompass(compass);
    m_mapView->locationDisplay()->start();
    connect(m_mapView->locationDisplay(), SIGNAL(locationChanged(Esri::ArcGISRuntime::Location)),
            this, SLOT(onLocationChanged(Esri::ArcGISRuntime::Location)));
}

/*!
 * \brief ArcGISSample::onLocationChanged
 * \param location the Location set on the MapView's LocationDisplay
 */
void ArcGISSample::onLocationChanged(Esri::ArcGISRuntime::Location location)
{
    // Center it once
    if(!m_centeredOnLocation)
    {
        m_mapView->setViewpointCenter(location.position(), 100000);
        m_lat->setText(tr("Latitude: %1").arg(QString::number(location.position().y())));
        m_long->setText(tr("Longitude: %1").arg(QString::number(location.position().x())));
        m_centeredOnLocation = true;
    }
}
