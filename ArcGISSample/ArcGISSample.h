// Copyright 2016 ESRI
//
// All rights reserved under the copyright laws of the United States
// and applicable international laws, treaties, and conventions.
//
// You may freely redistribute and use this sample code, with or
// without modification, provided you include the original copyright
// notice and use restrictions.
//
// See the Sample code usage restrictions document for further information.
//

#ifndef ARCGISSAMPLE_H
#define ARCGISSAMPLE_H

namespace Esri
{
namespace ArcGISRuntime
{
class Map;
class MapGraphicsView;
}
}

#include <QMainWindow>
#include <QVBoxLayout>
#include <QLabel>
#include "LocationDisplay.h"

class ArcGISSample : public QMainWindow
{
    Q_OBJECT
public:
    ArcGISSample(QWidget *parent = nullptr);
    ~ArcGISSample();

public slots:

private slots:
    void startLocation();
    void onLocationChanged(Esri::ArcGISRuntime::Location location);
private:
    Esri::ArcGISRuntime::Map*                   m_map;
    Esri::ArcGISRuntime::MapGraphicsView*       m_mapView;
    bool                                        m_centeredOnLocation;
    QWidget*                                    m_centralWidget;
    QVBoxLayout*                                m_layout;
    QLabel*                                     m_long;
    QLabel*                                     m_lat;
};

#endif // ARCGISSAMPLE_H
