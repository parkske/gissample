# README #

A coding sample using Esri's ArcGIS Runtime SDK for Qt.

### How do I get set up? ###

* Download and install Qt
* Download and install ArcGIS Runtime SDK for Qt
* Download Google Location history as JSON
    * Log in to Google
    * Download a range of data as JSON format:
        * [https://takeout.google.com/settings/takeout/custom/location_history](https://takeout.google.com/settings/takeout/custom/location_history)
* Open JSON file with the GoogleLocationHistory program and plot every point on a map

Copyright Kevin Parks 2017